import Adress from './Components/Profile/Adress'
import FullName from './Components/Profile/FullName'
import ProfilePhoto from './Components/Profile/ProfilePhoto'
import './App.css';

function App() {
  
  return (
    <div className="App">
      <div className='row '>
        <div className='col-8'>
          <ProfilePhoto/>
        </div>
      </div>
      <div className='row '>
        <div className='col-8'>
          <FullName/>
        </div>
      </div>
      <div className='row '>
        <div className='col-8'>
          <Adress/>
        </div>
      </div>
    </div>
  );
}

export default App;
