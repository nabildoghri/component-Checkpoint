import photo from '../../img/dbz.jpg'
function ProfilePhoto(){
    return (
        <div className='round'>
            <img src={photo} alt='my self' className="photo"/>
        </div>
    );
}

export default ProfilePhoto;